// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator
#include <deque> // deque

#include "gtest/gtest.h"

#include "Allocator.hpp"

using namespace testing;
using namespace std;

// -----------------
// AllocatorFixture1
// -----------------

template <typename T>
struct AllocatorFixture1 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_1 =
    Types<
    std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1);

TYPED_TEST(AllocatorFixture1, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture1, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// -----------------
// AllocatorFixture2
// -----------------

template <typename T>
struct AllocatorFixture2 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_2 =
    Types<
    my_allocator<int,    1000>,
    my_allocator<double, 1000>>;

TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2);

TYPED_TEST(AllocatorFixture2, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture2, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TYPED_TEST(AllocatorFixture2, test2) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    const pointer   b1 = x.allocate(s);
    const pointer   b2 = x.allocate(s);
    const pointer   b3 = x.allocate(s);

    x.deallocate(b1, s);
    x.deallocate(b2, s);

    const pointer b4 = x.allocate(2 * s);
    ASSERT_EQ(b < b4 && b4 < b3, true);
    x.deallocate(b, s);
    x.deallocate(b3, s);
    x.deallocate(b4, s);
}                                         // fix test

TYPED_TEST(AllocatorFixture2, test3) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    const pointer   b1 = x.allocate(s);
    const pointer   b2 = x.allocate(s);
    const pointer   b3 = x.allocate(s);
    auto iter = x.begin();

    ++iter;
    ++iter;
    char* c = reinterpret_cast<char*>(&*iter);
    c += sizeof(int);


    ASSERT_EQ(b2, reinterpret_cast<pointer>(c));
    x.deallocate(b, s);
    x.deallocate(b1, s);
    x.deallocate(b2, s);
    x.deallocate(b3, s);
}                                         // fix test

//=========================================================================


template <typename T>
struct AllocatorFixture3 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_3 =
    Types<
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

TYPED_TEST_CASE(AllocatorFixture3, allocator_types_3);

TYPED_TEST(AllocatorFixture3, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const pointer   b1 = x.allocate(0);


    ASSERT_EQ((b1 == nullptr), true);
}




TYPED_TEST(AllocatorFixture3, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const pointer    b = x.allocate(1);
    const pointer   b1 = x.allocate(0);
    const pointer   b2 = x.allocate(1);
    deque<pointer> unordered;
    unordered.push_back(b);
    unordered.push_back(b2);


    deque<pointer> allocate_deque;


    for(int i = 0; i < 2; ++i) {
        auto begin = unordered.begin();
        auto end = unordered.end();
        auto smallest_num = begin;
        auto min = *begin;
        while(begin != end) {
            if(*begin < min) {
                min = *begin;
                smallest_num = begin;
            }
            ++begin;
        }
        allocate_deque.push_back(*smallest_num);
        unordered.erase(smallest_num);
    }

    auto allocated_deque_begin = allocate_deque.begin();
    ASSERT_EQ((*allocated_deque_begin == nullptr), false);
    pointer delete_me = *allocated_deque_begin;
    x.deallocate(delete_me, s);
    allocate_deque.erase(allocated_deque_begin);

    allocated_deque_begin = allocate_deque.begin();

    ASSERT_EQ((*allocated_deque_begin == nullptr), false);
    x.deallocate(*allocated_deque_begin, s);
    ++allocated_deque_begin;
    auto allocated_deque_end = allocate_deque.end();
    ASSERT_EQ((allocated_deque_begin == allocated_deque_end), true);

}

TYPED_TEST(AllocatorFixture3, test2) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(1);
    const pointer   b1 = x.allocate(0);
    const pointer   b2 = x.allocate(1);

    x.construct(b, v);
    x.construct(b2, v);

    ASSERT_EQ(*b, v);
    ASSERT_EQ(*b2, v);

    x.deallocate(b, s);
    x.deallocate(b2, s);
}


struct combine_object {
    long l;
    double d;
    int i;
};

template <typename T>
struct AllocatorFixture4 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_4 =
    Types<my_allocator<combine_object, 100>>;

TYPED_TEST_CASE(AllocatorFixture4, allocator_types_4);

TYPED_TEST(AllocatorFixture4, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const pointer    b = x.allocate(1);
    const pointer   b1 = x.allocate(0);

    ASSERT_EQ((b == nullptr), false);
    ASSERT_EQ((b1 == nullptr), true);

    x.deallocate(b, s);
    x.deallocate(b1, s);
}
