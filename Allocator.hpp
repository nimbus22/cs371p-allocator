// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cmath>
#include <vector>
#include <deque>
#include <iostream>
#include <algorithm>
#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

using namespace std;

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     */
    bool valid () const {
        // <your code>
        // <use iterators>
        const_iterator iter_start = begin();
        const_iterator iter_end = end();
        int index = 0;

        bool positive = false;

        while(iter_start != iter_end) {
            if(*iter_start > 0 && positive) { //if the two block are positive
                return false;
            } else if(*iter_start > 0) { //you are here bc the left is neg and right is pos
                positive = true;
            } else { //you are here bc you are currently at a neg
                positive = false;
            }
            index += sizeof(int) + abs(*iter_start);//goes to the second sentinel
            if(*iter_start != (*this)[index]) { //check the value in the first sentinel == second sentinel
                return false;
            }

            index += sizeof(int);//updates the index to keep up with iter_start
            ++iter_start;
        }
        return true;
    }

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return &*lhs == &*rhs;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
            //iterator((*this)[0])
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }           // replace!

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            // <your code>
            int skip_num_block = abs(*_p) + 8;
            char* char_p = reinterpret_cast<char*>(_p);
            char_p += skip_num_block;
            _p = reinterpret_cast<int*>(char_p);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            int skip_num_block = *(_p - 1);
            _p -= (abs(skip_num_block) + 8)/4;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            // <your code>
            return  &*lhs == &*rhs;
        }                                                       // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p; //=====================


    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) { //=========================
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            // <your code>
            // static int tmp = 0;
            const int& result = *_p;
            return result;
        }                 // replace!

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            // <your code>
            int skip_num_block = abs(*_p) + 8;
            const char* char_p = reinterpret_cast<const char*>(_p);
            char_p += skip_num_block;
            _p = reinterpret_cast<const int*>(char_p);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            // <your code>
            int skip_num_block = *(_p - 1);
            _p -= (abs(skip_num_block) + 8)/4;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        // (*this)[0] = 0; // replace!
        // <your code>
        (*this)[0] = N - (2 * sizeof(int));
        (*this)[N - sizeof(int)] = N - (2 * sizeof(int));
        if(N < sizeof(T) +(2 * sizeof(int))) {
            throw bad_alloc();
        }

        assert(valid());
    }
    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type num_objects) {
        // <your code>
        if(num_objects == 0) {
            return nullptr;
        }
        pointer result = nullptr;
        int num_of_bytes = num_objects * sizeof(T); //num of bytes w/o sentinels
        iterator iter_begin = begin();
        iterator iter_end = end();
        int index = 0;

        while(iter_begin != iter_end && *iter_begin < num_of_bytes) { //positive number, free block
            index += abs(*iter_begin) + 2 * sizeof(int);
            ++iter_begin;
        }
        if(iter_begin == iter_end) { //if there are no spaces available throw an exception
            throw bad_alloc();
        }

        result = reinterpret_cast<pointer>(&(*this)[index + sizeof(int)]);
        //^^ maybe change to "a + index + sizeof(int)"?
        int old_val = *iter_begin; //old sentinel
        int check = num_of_bytes + 4 * sizeof(int) + sizeof(T);
        if(old_val + 2 * sizeof(int) < check) { // cant split block not cannot fit 2 sentinel and a sizeof(T)
            //we want to give the whole block aka just update the old sentinel to negative
            *iter_begin *= -1; //makes the left sentinel negative
            index += sizeof(int) + old_val; //updates the index to the right sentinel
            (*this)[index] *= -1; //makes the right sentinel negative


        } else {
            //if we are here we split the block
            *iter_begin = num_of_bytes * -1;
            index += sizeof(int) + num_of_bytes;
            (*this)[index] = num_of_bytes * -1;
            index += 4;


            old_val = (old_val - num_of_bytes) - 2 * sizeof(int);//recalculate the old val
            (*this)[index] = old_val; //set up the new sentinel
            index += sizeof(int) + old_val;
            (*this)[index] = old_val;
        }



        assert(valid());
        return result;
    }             // replace!

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p, size_type t_size) {
        // <your code>
        if (p == nullptr) return;
        pointer begin_array =reinterpret_cast<pointer>(a);
        pointer end_array = reinterpret_cast<pointer>(a + N);
        if(p < begin_array || p > end_array) {
            throw invalid_argument("invalid pointer p: " + to_string(reinterpret_cast<long int>(p)));
        }

        bool left_free = false;
        bool right_free = false;
        char* p_begin = reinterpret_cast<char*>(p);
        p_begin -= sizeof(int);//goes to left sentinel
        *reinterpret_cast<int*>(p_begin) *= -1; //makes left sentinel positive
        char* p_end = p_begin + *reinterpret_cast<int*>(p_begin) + sizeof(int);//goes to the right sentinel
        *reinterpret_cast<int*>(p_end) *= -1;//makes right sentinel positive
        int free_space = *reinterpret_cast<int*>(p_begin);

        if(p_begin != a) {
            p_begin -= sizeof(int);
            if(*reinterpret_cast<int*>(p_begin) > 0) {
                left_free = true;
                free_space += 2 * sizeof(int) + *reinterpret_cast<int*>(p_begin);
            }
        }
        if(p_end + sizeof(int) != a + N) {
            p_end += sizeof(int);
            if(*reinterpret_cast<int*>(p_end) > 0) {
                right_free = true;
                free_space += 2 * sizeof(int) + *reinterpret_cast<int*>(p_end);
            }
        }
        // merging left block with current block, or merging both left and right blocks
        if(left_free) {
            p_begin -= (sizeof(int) + *reinterpret_cast<int*>(p_begin));//make p_begin to the left most sentinel
            *reinterpret_cast<int*>(p_begin) = free_space;
            p_end = p_begin + free_space + sizeof(int);
            *reinterpret_cast<int*>(p_end) = free_space;
        }
        //merging current block with right block
        else if(right_free) {
            if(p_begin != a) {
                p_begin += sizeof(int);
            }
            *reinterpret_cast<int*>(p_begin) = free_space;
            p_end = p_begin + sizeof(int) + free_space;
            *reinterpret_cast<int*>(p_end) = free_space;
        }



        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&((*this)[0]));
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&((*this)[0]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&((*this)[N]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&((*this)[N]));
    }
};

#endif // Allocator_h
