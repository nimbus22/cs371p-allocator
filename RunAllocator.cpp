// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    read *
    evalated *
    printed *
    looped *
    */
    int test_cases;
    cin >> test_cases;
    cin.ignore(); //ignores \n
    cin.ignore(); //ignores blank line
    string string_input;

    for(int i = 0; i < test_cases; ++i) {
        my_allocator<double, 1000> allocator1;
        deque<double*> allocated_deque;
        //we use getline to differentiate between empty lines and non empty lines
        getline(cin, string_input);

        while(!string_input.empty() && !cin.fail()) { //if line is empty or we have reach the end
            int int_input = stoi(string_input);
            if(int_input > 0) { //allocate
                double* pointer = allocator1.allocate(int_input);
                deque<double*>::iterator deque_cur = allocated_deque.begin();
                deque<double*>::iterator deque_end = allocated_deque.end();
                while(deque_cur != deque_end && pointer > *deque_cur) {
                    ++deque_cur;
                }
                allocated_deque.insert(deque_cur, pointer);
            } else if (int_input < 0) { //deallocate
                int input_pos = abs(int_input) - 1;
                if (allocated_deque.size() - 1 >= input_pos) {
                    allocator1.deallocate(allocated_deque.at(input_pos), sizeof(double));
                    deque<double*>::iterator deque_begin = allocated_deque.begin() + input_pos;
                    allocated_deque.erase(deque_begin);
                }
            }
            getline(cin, string_input);//gets the next line
        }
        my_allocator<double, 1000>::iterator iter_begin = allocator1.begin(); //try to make it a const_iterator=========
        my_allocator<double, 1000>::iterator iter_end = allocator1.end(); //try to make it a const_iterator=============
        while(iter_begin != iter_end) { //prints out everything
            cout << *(iter_begin++) << " \n"[iter_begin == iter_end];
        }
    }

    return 0;
}